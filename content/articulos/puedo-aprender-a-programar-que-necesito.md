---
title: "¿Puedo aprender a programar? ¿Qué necesito?"
slug: "puedo-aprender-a-programar-que-necesito"
description: "Respuestas a la pregunta de si puedes aprender a programar, qué hace falta y qué habilidades son necesarias para conseguirlo."
date: 2022-02-25
publishdate: 2022-02-25
lastmod: 2022-02-25
draft: false
# Taxonomies
categories:
  - "Desarrollo"
authors:
  - "josejfernandez"
# User-Defined
tableOfContents: true
---

Si estás leyendo estas líneas y estás pensando en aprender a programar, formas parte del **pequeño grupo que decide aprender a dominar la tecnología** en un mundo en el que la tecnología nos domina a nosotros. Hay muchos incentivos para aprender, desde que prácticamente todo en el mundo actual está gobernado por programas al hecho de que programar es una actividad laboral muy demandada y bastante lucrativa.

Sea cual sea tu motivación, tal vez te hayas preguntado si *realmente* puedes aprender a programar. Vamos a darle respuesta en este artículo.

## 1. ¿Quién puede aprender a programar?

Actualmente **cualquiera puede aprender a programar**. Hace tiempo que [no hace falta un título para aprender]({{< relref "#3-hace-falta-tener-título-para-programar" >}}), y muchas veces ni siquiera para trabajar como programador, porque la demanda es tan alta que **las empresas no consiguen encontrar suficientes programadores** con título.

Hoy en día existen mucho material disponible para aprender a programar. Tienes a tu alcance multitud de cursos, videos, artículos y tutoriales que te ayudarán a aprender, siendo todos ellos gratuitos de forma que **tampoco es cuestión de dinero**.

El principal recurso que necesitas para aprender a programar es **tiempo** y, por tanto, cierta dosis de paciencia. Si por tus circunstancias careces de tiempo, no puedes conseguirlo o prefieres no invertirlo en esto, puedes intentarlo pero entiende que tus progresos serán, inevitablemente, bastante más lentos.

Es muy importante que entiendas que prender a programar [no es difícil]({{< relref "#6-es-fácil-aprender-a-programar-cuánto-tiempo-lleva" >}}), pero require un esfuerzo activo por tu parte. No basta con leer un artículo o ver un tutorial: tienes que ponerlo en práctica.

Ponerlo en práctica es fácil, y mucha gente lo encuentra divertido porque supone experimentar y explorar, además de que cometer errores **no tiene consecuencias** casi nunca. ¡Eres libre de cometer errores y aprender de ellos! Además, es muy gratificante porque desde el principio estarás construyendo con tus manos y podrás ver tus progresos muy fácilmente. Pronto mirarás atrás y verás lo que has avanzado.

No obstante, en el proceso te surgirán dudas -como es normal- que te impedirán avanzar, frustrándote, haciendo que dudes de tu capacidad y que pienses en abandonar.

**➡ Nunca dudes de tu capacidad por no saber algo. Nadie nace aprendido.**

No tirar la toalla será súper importante desde el principio, porque **cada duda es una oportunidad de aprender**. Tendrás que buscar respuestas, ya sea preguntando o investigando por tu cuenta, por lo que **la motivación y la constancia** jugarán un papel fundamental.

Ayudará disponer de una comunidad de gente que te ayude en tu camino. La encontrarás [en nuestro foro](https://foro.secretosdelsoftware.com) 🙂

## 2. ¿Hace falta saber matemáticas para aprender a programar?

En general, **no**.

Como veremos más adelante en el artículo, hay habilidades más importantes para programar que las matemáticas, como la lógica o la capacidad de descomponer un problema grande en partes más pequeñas.

No obstante, programar es algo muy general. Cuando programamos, le estamos indicando al ordenador qué pasos debe seguir para hacer lo que queramos (si te fijas, esto básicamente es descomponer una tarea que tú haces en partes pequeñas que el ordenador puede hacer). Si la tarea que quieres que el ordenador haga implica matemáticas, entonces evidentemente necesitas saber matemáticas. De lo contrario no serás capaz de indicarle al ordenador lo que debe hacer, porque ni siquiera tú sabes hacerlo.

En resumen: parece lógico, pero tienes que saber hacer (o cómo hacer) lo que quieres que el ordenador haga por ti.

## 3. ¿Hace falta tener título para programar?

Puedes programar **sin necesidad de tener ningún título**.

Si te estás preguntando si puedes **trabajar en una empresa** como programador sin tener ningún título, la respuesta es que *depende*. Depende de la empresa y del puesto. Algunas empresas requieren que tengas título y otras no. Algunas piden un título universitario y otras se conforman con un título inferior (en España, por ejemplo, de *Formación Profesional*). Algunas valoran si tienes título y otras se fijan menos (o nada). Algunas piden título para acceder a determinados puestos y para otros no.

Entonces, ¿tengo posibilidades *reales* de trabajar en esto sin título? Sí, las tienes, pero depende de la empresa.

Si hablamos de las posibilidades de **promoción** volvemos al mismo escenario: depende de la empresa y del puesto. Pero en este caso debes tener en cuenta que una vez has accedido a un puesto de trabajo, la experiencia (tanto en cantidad como en calidad) que tienes gana mucha relevancia frente a los estudios, porque, bueno, ya te están pagando en una empresa, ¿no? Se entiende que cumples y que sabes. Además, en entornos de trabajo es posible que **la empresa te facilite obtener formación específica** para desarrollar las competencias que le interesen tanto a la empresa como al empleado, por lo que la educación previa que tuvieras sería aún menos relevante. Aunque esto, de nuevo, depende de la empresa.

¿Y si tuvieras **títulos no relacionados**, como biología, turismo, filología, arquitectura...? En ese caso estarías en una situación similar a "no tengo título", porque en tu formación muy probablemente no has adquirido conocimientos de desarrollo de software, pero contarías con la ventaja de que **tienes conocimientos en otras áreas**. Por ejemplo: una empresa desarrolla software para asesorías jurídicas y tú tienes estudios en derecho. Estás en mejor situación que, por ejemplo, alguien con estudios de biología.

¿Significa esto que **los títulos no sirven para nada**? ¡En absoluto! La formación en informática te ofrece muchos conocimientos que no siempre es muy evidente que te beneficien (no muchos en Formación Profesional estudian *patrones de diseño de software*, por ejemplo) así como experiencia práctica, mediante los programas de prácticas en empresas u otro tipo de acuerdos.

En resumen, los títulos no son imprescindibles. Nunca restan y siempre suman, pero su relevancia depende de la empresa. Si tienes la oportunidad **siempre será positivo que tengas estudios relacionados**, que por supuesto puedes complementar con autoformación.

Aunque al final muchas veces lo que marcará la diferencia será tu capacidad y **tu actitud para aprender más y llegar más lejos**.

## 4. ¿Qué habilidades hacen falta para ser programador?

El pensamiento lógico y la capacidad de razonar son importantísimas para un programador. Es decir, ser capaz de ahondar en por qué suceden las cosas, en base a qué, cómo se comportarían si se cambiara esto o aquello... Quizá te suene muy abstracto, pero siempre debes mantener viva la pregunta del millón: **¿Por qué?**

Cuando programas, es súper importante centrarse en los fundamentos y las causas. Vas a hablar con un ordenador, que en el fondo sólo entiende ceros y unos. Es una **máquina fría que hace *lo que le dices que haga*, no *lo que quieres que haga***. Hay una sutil diferencia entre las dos y muchas veces un simple caracter marcará la diferencia entre *esto es una mierda* y *va como la seda*.

Tienes que ponerte a su nivel y entender que la máquina es tonta. Sólo hace lo que le dicen. Por eso hay que decírselo en el lenguaje que entiende, con precisión absoluta. Si no lo haces así, la máquina no hará lo que quieres.

Una vez eres capaz de comunicarte con la máquina, la siguiente habilidad más relevante sin duda es la **capacidad de descomponer problemas o tareas complejas en otros más pequeños**. No necesitas tener esta capacidad *ahora*, sino que la puedes ir desarrollando a medida que aprendes. Es imposible abordar problemas complejos como un todo. Muchos principiantes cometen el error de hacerlo de ese modo y se desesperan porque no saben por dónde empezar.

Otra cosa a la que dedicarás bastante tiempo es a buscar respuestas, por lo que la capacidad, que también se puede desarrollar, de *separar el grano de la paja* será muy importante. Es decir, la capacidad de **leer, entender y discernir si algo es relevante o no** según tu caso concreto. Vivimos en un mundo en el que sobra información y no toda es interesante o se aplica a todos los casos.

Finalmente, la **capacidad de comunicarte con otros y de empatizar** es también fundamental. Debes poder transmitir tus ideas y tus pensamientos y hacérselos entender a otros, así como debatir sobre posibles soluciones, ventajas y desventajas o de argumentar por qué esto sí y por qué aquello no. ¿Pensabas que los programadores no se comunicaban? **Programas una máquina para que sirva a personas**, no lo olvides.

## 5. ¿Qué herramientas o programas necesito para programar?

Prácticamente nada en específico. Si tienes un ordenador puedes programar. No es necesario que uses ningún sistema operativo concreto, ni ningún programa en particular. En países menos desarrollados en África, **hay gente que programa con su teléfono móvil**.

Programar no es mucho más que **leer, escribir y modificar código**, que a su vez no es más que texto, como este artículo, salvo que el código necesita cumplir con unas normas estrictas de sintaxis.

Dicho eso, cuanto mayor sea la complejidad de aquello que quieras programar, probablemente mayores serán las capacidades que tiene que tener tu ordenador para servirte de forma efectiva, normalmente en forma de un procesador más rápido, más memoria RAM, una pantalla con un tamaño adecuado o un teclado que resulte cómodo de utilizar.

Si quisieras **desarrollar software para sistemas muy concretos**, como dispositivos de la marca Apple, *mainframes* ("los ordenadores de la NASA"), sistemas industriales o dispositivos concretos (una lavadora), necesitarías acceso a dichos sistemas. El acceso a muchos de esos dispositivos está fuera del alcance de muchas personas, pero existen otras herramientas que podrían ser de ayuda en ese caso, como las máquinas virtuales o los emuladores.

En último lugar, el **programa que utilices para escribir el código** puede ir desde un editor de texto, como Notepad++ o Gedit, a entornos de desarrollo (por sus siglas en inglés, IDE) como Visual Studio, IntelliJ IDEA o NetBeans. Si estás empezando, casi siempre serán gratuitos salvo que, como en el párrafo anterior, tengas unas necesidades muy concretas.

## 6. ¿Es fácil aprender a programar? ¿Cuánto tiempo lleva?

Buena pregunta, pero difícil de responder con exactitud. En general **aprender a programar no es demasiado difícil**, pero sí **requiere que te impliques**. No es algo que puedas aprender sólamente viendo videos, tienes que ponerte y llevarlo a la práctica.

Cuánto tiempo te lleve depende de muchos factores. Del tiempo de que dispongas, de los conocimientos previos que tengas, de si has estudiado algo relacionado, de si estás muy motivado o poco motivado, de si lo encuentras divertido o aburrido, de si aprendes por tu cuenta o si te enseñan (curso, Bootcamp, mentor), de si cuentas con una comunidad como [el foro de Secretos del software](https://foro.secretosdelsoftware.com) para ayudarte... 🙂

También **tus objetivos pueden influir**. Por ejemplo, si pretendes aprender desarrollo web frontend hay muchísima información disponible y gente que te podrá ayudar, por lo que en general será más fácil y rápido, pero si pretendes aprender a programar [SCADA](https://es.wikipedia.org/wiki/SCADA)s industriales probablemente será más difícil y más lento porque no encontrarás tanta información y tener acceso a ese tipo de sistemas es mucho más complicado.

Es muy importante mencionar que si has oído alguna vez a alguien (normalmente programadores) decir que *"es fácil aprender a programar"*, mi consejo es que no lo tomes muy en serio porque **puede crearte expectativas** que no se correspondan con tu situación. ¿Cómo pueden afirmar algo así sin conocer tu situación y tus objetivos?

Para que te hagas a la idea, hay bootcamps que duran unos meses y supuestamente van de cero a *"lo suficientemente bueno como para empezar a trabajar en una empresa"*. Un Ciclo Formativo de Grado Superior (Formación Profesional en España) dura 2 años, generalmente no asumen conocimientos previos y la inserción laboral es bastante buena. Una carrera universitaria no lleva menos de 4 años.

Podemos decir que siguiendo uno de esos programas formativos, **de aquí a 2 años** puedes estar trabajando de programador en una empresa.

¿Y si simplemente quieres hacer algo concreto, pero no trabajar en esto? Entonces lógicamente hablaríamos de bastante menos tiempo. Dependiendo de la dificultad de aquello que quieras hacer y de cómo de rápido aprendieras, podríamos decir que **desde unas semanas hasta unos meses**.

Por eso buena idea que, si quieres aprender, te pases por [nuestro foro](https://foro.secretosdelsoftware.com) y nos expongas tu situación. Seguro que alguien lee tu caso e intenta orientarte.

## 7. ¿Qué lenguaje de programación aprendo primero?

*Muy pronto dispondremos de un artículo completo dedicado a este apartado.*

## 8. ¿Cuál es la mejor forma de aprender a programar?

*Muy pronto dispondremos de un artículo completo dedicado a este apartado.*

## Anexo 1. Aprender a programar siendo discapacitado.

*Nota: por favor disculpa si no he utilizado el término adecuado. Siéntete libre de enviar una sugerencia de corrección al respecto.*

🙄 Nos gustaría ampliar esta sección. Si dispones de conocimientos para este apartado, por favor compártelos con nosotros.

## Anexo 2. Aprender a programar sin un ordenador.

🙄 Nos gustaría ampliar esta sección. Si dispones de conocimientos para este apartado, por favor compártelos con nosotros.
