*A website in Spanish that helps people learn about software, software development, privacy and digital rights.*

## El proyecto

Secretos del software es un sitio web que pretende ayudar a la gente a aprender acerca del software, desarrollo de software, privacidad y derechos digitales.

Aunque ya existen multitud de sitios web en Internet que tratan esta temática, principalmente lo hacen en inglés y además a un cierto nivel técnico que impide que las personas con menos conocimientos accedan a la información. Decidí crear Secretos del software para llenar ese pequeño vacío y para facilitar la divulgación de estos contenidos en nuestro idioma a personas con un perfil menos técnico.

## Descripción técnica

El sitio web está desarrollado con [Hugo](https://gohugo.io/), un generador de sitios web estáticos (sólo HTML, CSS y JavaScript) muy potente, y utilizamos [Bootstrap](https://getbootstrap.com/) como framework CSS. Por el momento no utilizamos JavaScript.

Los contenidos se escriben en [Markdown](https://es.wikipedia.org/wiki/Markdown), que es un lenguaje muy sencillo para dar formato al texto plano de una forma muy visual. Su sintaxis es muy simple y no tiene nada que ver con HTML.

Hemos dividido los contenidos en varias secciones: digitalización (aprender a usar software), desarrollo (aprender a crear software), privacidad (aprender a protegerse del software) y derechos digitales (aprender tus derechos en un mundo de software). Hay cabida para artículos, guías, noticias y posts de blog.

## Cómo colaborar

Buscamos ayuda para las siguientes tareas:

- Desarrollo del sitio web.
- Redacción de contenidos para el sitio web.
- Colaborar con el foro.

> Si quieres ver una lista de temas con los que puedes empezar a ayudar, mira [nuestra lista de issues](https://codeberg.org/josejfernandez/secretosdelsoftware.com/issues) y, en particular, aquellos etiquetados como *[Fácil](https://codeberg.org/josejfernandez/secretosdelsoftware.com/issues?labels=37725)*. No olvides que es importante contactar con nosotros (apartado *"Medios de contacto"* más abajo). Sigue leyendo para saber más acerca de cada unas de las áreas.

### Desarrollo del sitio web

Para colaborar con el desarrollo deberás tener conocimientos de HTML y CSS, e idealmente también de Bootstrap. No es necesario que sepas cómo usar Hugo, el generador de sitios que utilizamos, pero deberás familiarizarte con él y poder leer su documentación (en inglés). No es necesario que seas un crack en nada de lo anterior. Si tienes ganas, ese es un excelente punto de partida.

Aunque mucha gente piense lo contrario, el desarrollo es una actividad de equipo que exige bastante comunicación. Siempre podrás preguntar las dudas que tengas o pedir orientación y te ayudaremos. Además, es una buena forma de poner en práctica conocimientos o de adquirir nuevos, por lo que, estés en el nivel que estés, te damos la bienvenida 🙂 Antes de empezar, eso sí, te sugiero que leas nuestra documentación y que te pongas en contacto con nosotros (apartado *"Medios de contacto"* más abajo) para saludar y para saber por dónde empezar y con qué tareas necesitamos ayuda en cada momento.

### Redacción de contenidos para el sitio web

Para colaborar con contenidos es recomendable que contactes en primer lugar con nosotros (apartado *"Medios de contacto"* más abajo). De esta manera podremos hablar acerca de qué contenidos tienes intención publicar y encaminar la colaboración. Además de publicar contenidos nuevos, también puedes colaborar corrigiendo, editando o ampliando los ya existentes, si te interesa. Si participas en la elaboración de un contenido con otras personas, siempre serías mencionado como co-autor.

No es necesario que tengas conocimientos técnicos específicos, pero sí que puedas manejarte bien con la tecnología, pues tendrás que utilizar herramientas técnicas (tus contribuciones serán mediante *pull request*, más abajo explicamos qué hace falta y cómo se hace) y tendrás que informarte en un medio eminentemente técnico y cuyos contenidos están habitualmente en inglés. Entre las tareas que puedes esperar están escribir, formatear texto y realizar edición básica de imágenes, entre otras cosas.

### Colaborar con el foro

Colaborar con el foro es más fácil porque no requiere ninguna herramienta específica. Accede al foro en https://foro.secretosdelsoftware.com, regístrate y empieza a colaborar. La mejor manera de hacerlo es contribuyendo creando debates, compartiendo información y participando en los debates que creen otros usuarios. Aún así es recomendable que contactes con nosotros mediante alguno de los medios de que disponemos si quieres colaborar de esta manera.

## Proceso para colaborar

Para colaborar con el sitio web son necesarias las siguientes herramientas.

- Registrarte en [Codeberg](https://codeberg.org/) (el servicio de alojamiento de código que usamos).
- Un editor de texto o IDE tu elección, como [VSCodium](https://vscodium.com/). *Todo es texto plano. No usamos ninguna suite ofimática.*
- [Git](https://git-scm.com/). *Puedes utilizar la herramienta que quieras con Git, como tu IDE, la consola o algún cliente específico como SourceTree.*
- [Nodejs](https://nodejs.org/en/). *Si no lo tienes ya instalado en tu sistema, es recomendable que instales la versión LTS (soporte extendido)*.

¿Empezamos?

1. Instala las herramientas mencionadas anteriormente y [la versión extendida de Hugo](https://github.com/gohugoio/hugo/releases).
2. Clona este repositorio. Si usas la consola: `git clone https://codeberg.org/josejfernandez/secretosdelsoftware.com.git`
3. Abre una ventana de consola y navega hasta el directorio donde clonaste el repositorio.
4. Ejecuta `npm install` para instalar las dependencias del front end.
5. Ejecuta `hugo server -D` para ejecutar el servidor de desarrollo.
6. Accede a http://localhost:1313 para ver el sitio web mientras lo modificas.

Con las modificaciones que realices deberás enviar una *pull request*. Es recomendable que expliques lo mejor posible qué has hecho (enlazando cuando corresponda a los *issues* existentes), por qué lo has hecho y que mantengas la lista de cambios lo más reducida posible, para que sea fácil y rápido evaluarla.

Si tus cambios son aceptados, ¡excelente! Pero, ¿y si no? Empezará una conversación en la que se explicará por qué no y qué deberías hacer para conseguir que incorporemos tus cambios. El diálogo y la colaboración son piezas clave, por lo que no desesperes si tus cambios no son aceptados a la primera.

## Código de conducta para colaboradores

- **Sé respetuoso.** El respeto es fundamental para que las personas se sientan seguras y cómodas. No hay ningún motivo válido para desvirtuar un ambiente de colaboración. No siempre estaremos de acuerdo y no siempre nos gustarán los puntos de vista o las decisiones de los demás, pero nunca debemos permitir que el desacuerdo o la frustración se tornen en hostilidad hacia los demás. ¿Te has parado a intentar comprender por qué la otra persona piensa de modo diferente?
- **Sé colaborativo.** Vivimos en un mundo complejo en el que el esfuerzo individual de una única persona tiene un recorrido más corto que largo. Hagamos lo que hagamos, la mejor forma de hacerlo y la más efectiva es colaborando con otros. No queremos cowboys ni lobos solitarios. Si todos remamos en la misma dirección, llegaremos más lejos.
- **Sé comunicativo.** TBD
- **Sé legal.** TBD
- **Pregunta. Pide ayuda.** Nadie lo sabe todo. Cuando dudes o cuando no sepas cómo continuar es buena idea implicar a otra persona para que nos aporte un nuevo punto de vista. Las buenas preguntas tienden a ayudar tanto a quien las hace como a quien las contesta. Puede que tu pregunta ya haya sido respondida y está bien que lo averigües antes de preguntar, pero ante la duda es mejor hacer la pregunta que callársela. En el peor de los casos, una persona aprenderá algo.
- **Piensa en el usuario final.** Si estás leyendo esto probablemente tienes más conocimientos técnicos que la mayoría de la gente. Eso te coloca en una posición de responsabilidad: recuerda que los consejos que damos, las opiniones que vertimos y los comentarios que hacemos influyen en otras personas. Siempre debemos intentar que esa influencia sea positiva y útil para quien la recibe, y siempre debemos poner al usuario final en el centro de nuestros procesos.

## Medios de contacto

- Canal de Telegram: https://t.me/secretosdelsoftware

## Licencias

Los contenidos en el directorio `/content` se publican bajo una licencia [Creative Commons 4.0 BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) ([texto completo de la licencia](https://codeberg.org/josejfernandez/secretosdelsoftware.com/src/branch/main/LICENSE-CONTENTS)). El código del repositorio se publica bajo una licencia [MIT](https://opensource.org/licenses/MIT) ([texto completo de la licencia](https://codeberg.org/josejfernandez/secretosdelsoftware.com/src/branch/main/LICENSE-CODE)). El nombre 'Secretos del software' y el logo son propiedad de Jose J. Fernández (todos los derechos reservados).
