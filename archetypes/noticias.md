---
title: "{{ replace .Name "-" " " | title }}"
#slug: ""
description: ""
date: {{ .Date }}
publishdate: {{ .Date }}
lastmod: {{ .Date }}
draft: true
# Taxonomies
categories:
  - ""
authors:
  - ""
# User-Defined
tableOfContents: false
---
