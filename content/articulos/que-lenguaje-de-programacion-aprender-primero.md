---
title: "¿Qué lenguaje de programación debo aprender para empezar?"
slug: "que-lenguaje-de-programacion-aprender-primero"
description: "Este artículo te ayudará a decidir qué lenguaje de programación aprender para empezar, de forma personalizada para ti, para que puedas conseguir tus objetivos."
date: 2022-02-25
publishdate: 2022-02-25
lastmod: 2022-02-25
draft: true
# Taxonomies
categories:
  - "Desarrollo"
authors:
  - "josejfernandez"
# User-Defined
tableOfContents: true
---
