---
title: "Git: introducción"
slug: "git-introduccion"
description: "Introducción a qué es Git, cómo usarlo y una breve explicación de alguno de sus comandos más útiles"
date: 2022-03-13
publishdate: 2022-03-13
lastmod: 2022-03-13
draft: false
# Taxonomies
categories:
  - "Desarrollo"
authors:
  - "manuelperez"
# User-Defined
tableOfContents: true
---

## ¿Qué es Git?
Git es un software de control de versiones, creado por Linus Torvalds, el creador del sistema operativo Linux.

Pero, ¿Qué es eso de **software de control de versiones**. Según una definición de Internet, sería esto:

> Una herramienta de software que monitoriza y gestiona cambios en un sistema de archivos. Asimismo, un VCS ofrece herramientas de colaboración para compartir e integrar dichos cambios en otros usuarios del VCS

[Origen definición](https://bitbucket.org/product/es/version-control-software)

Para ponerlo de forma sencilla es un programa que nos permite compartir código en un mismo proyecto, de forma que yo puedo ir trabando en una parte y mi compañero en otro. Luego, podemos unir nuestros esfuerzos en un único lugar y mantener de esa forma, un proyecto base con todas las actualizaciones y cambios.

Pero eso no es todo lo que nos ofrece un sistema de versiones. Podríamos pensar, que tiene dos grandes ventajas. La primera, como he mencionado, que facilita el trabajo en grupo. La segunda, es que te facilita el trabajo en loca.

Si has programado alguna vez, seguro que en alguna que otra ocasión te has cargado un archivo, que ya no puedes recuperar con el comando deshacer. O has tenido que hacer varias pruebas de cuál puede ser la mejor fórmula para resolver uno u otro problema... Y claro, ahora toca ir probando, comentando código antiguo para no perderlo, por si la nueva fórmula no funciona. ¿No es eso un lío?

Pues el sistema de control de versiones también nos ayuda con eso. Pues podemos tener **diferentes** versiones de un mismo proyecto. Digamos que podría sacar una instantánea de cómo está actualmente el proyecto, hacer todos los cambios que quiera y si no me gusta, puedo restaurar el proyecto a cómo estaba antes.

Eso y muchas más cosas nos ofrece un sistema de control de versiones, en este caso, Git, es uno de los más usados.

## Ramas
He mencionado anteriormente que podemos tener varias versiones de un mismo proyecto, pues eso son las ramas o, en inglés, *branchs*. Digamos que son instantáneas de un momento dado de nuestro proyecto. Dichas ramas salen del proyecto base, o rama base y podemos tener tantas como queramos. 

De esa forma, podríamos estar trabajando en nuestro proyecto en la rama master o main (que suele ser el nombre más común para la rama principal). Y crear una nueva rama para desarrollar una nueva funcionalidad o probar a cambiar alguna parte de nuestro código, sin miedo a modificar el archivo (o proyecto) base.

Luego, si los cambios nos resultaron adecuados podemos integrar dichos cambios creados en la nueva rama, en la rama principal y volver a trabajar en dicha rama.

De esa forma, podemos mantener un flujo ordenado y seguro de trabajo.

## Comando Git
Ahora bien, en esta introducción a Git no vamos a ver cómo instalarlo, pero sí os voy a hablar sobre los comandos básicos de esta herramienta de control de versiones.

Cabe recalcar que dichos comandos se ejecutan en la consola del Sistema Operativo en cuestión, por ejemplo la *cmd* de Windows; pero, para ello, será necesario tener instalado previamente Git.

### git init

Este comando solo se ejecuta una vez, al iniciar el proyecto, para poder ir tomando instantáneas.

Este comando crea dos áreas donde irá almacenando los archivos:

* **Staging area (área de ensayo):**
    * Área temporal donde se almacenan los archivos temporalmente.
* **Local repository (repositorio local):**
    * Se almacenan las instantáneas de forma permanente.

### git add

Sirve para dar seguimiento a los archivos del proyecto. Puede ser a uno, a varias o a todos. Este comando lleva al archivo del área de trabajo al área de ensayo.

* **git add nombreArchivo.*
    * Nos permite añadir un archivo, a partir del nombre, al staging area.

### git commit

Este comando traslada los archivos del área de ensayo al repositorio local, tomando así una instantánea con la que se podrá trabajar a posteriori.

Si se nos olvida agregar al área de estado los archivos (git add), no se agregarán los cambios hecho a la instantánea.

* **git commit -m**
    * Crea una instantánea y le da una descripción.

### git status -s

Nos da información sobre los archivos dentro del proyecto y su estado actual. Si tienen cambios que se deban guardar, si tienen seguimiento o si no lo tienen, etc.

### git log

Este comando nos permite obtener una ristra de información sobre los commits que existen en el proyecto. El identificador de cada uno, el autor, la fecha del commit, entre otras cosas.

* **git log --oneline**
    * Muestra la información "en una línea", mostrando el código de identificación, el commit actual, la descripción, la rama en local y en "origin".

## git reset

Este comando permite volver a un commit anterior, para lo cual, necesitaremos el nombre del identificador de dicho commit (se puede obtener con git log).

* **git reset --hard identificadorCommit**
    * Devuelve a un estado anterior (a la instantánea del commit anterior), los archivos.

## git remote

Permite interactuar con el repositorio "remoto", es decir, en GitHub, por ejemplo.

* **git remote add origin url**
    * Permite vincular el contenido de un proyecto que tengamos a un repositorio de en la nube, definido por la url.

## git push

Permite enviar el contenido que tenemos en "local" a un repositorio "online". Puede verse como el git commit, pero para "subir" archivos al servidor.

* **git push -u origin master**
    * Sube el contenido que tenemos en origin (repositorio local) a master (repositorio en la nube) a una rama concreta.
* **git push --tags**
    * Sube al repositorio "online" todas las tags que se hayan creado.

## git pull

Este comando descarga a local, cualquier cambio que se haya realizado en el proyecto "online", es decir, por ejemplo, si modificamos algún archivo desde el propio git hub, podremos hacer git pull para sincronizar nuestro proyecto local con el modificado en la nube.

## git tag

Podemos ver las *tags* como marcadores de versiones (o el uso que queramos darle), pero vienen a determinar una instantánea de nuestro proyecto que, por cualquier motivo, es relevante. Por ejemplo, la versión 1 de nuestro proyecto, podría merecer un tag. La siguiente versión, otra. De esa forma, podemos tener organizadas las diferentes versiones del proyecto y, tener así, fácil acceso a dichas versiones.

* **git tag título -m "descripción"**
    * Esta versión crea una tag con un título y una descripción dada.

## git clone

Este comando nos permite descargar "clonar" una instantánea de un proyecto online. Si el proyecto es nuestro, podremos editarlo y seguir subiendo modificaciones.

* **git clone urlOrigen urlDestino(?)**
    * La urlOrigen es obligatoria y determina dónde está localizado el proyecto que queremos copiar. La urlDestino no es obligatoria y si no la ponemos el proyecto se copiará donde estemos situados en la consola.

## git branch

Con este comando podemos crear una bifurcación/rama en nuestro proyecto, de forma que podamos trabajar en dos versiones (o más) diferentes del mismo, sin, a priori, afectar a la otra versión/corriente de trabajo.

* **git branch**
    * Muestra todas las ramas del proyecto y en cuál estamos actualmente trabajando (con un asterisco).
* **git branch nombreRama**
    * Crea una nueva rama con el nombre dado.
* **git branch -d nombreRama**
    * Permite eliminar una rama con nombre dado. Puede ser una opción interesante cuando hacemos un merge y sabemos que ya no vamos a trabajar con una rama en concreto.

## git merge

Permite unir dos ramas/flujo de trabajo en uno solo, siempre que hayan surgido de la misma rama. Es decir, si tenemos una rama llamada A1 y de dicha rama sale B1 y A1, podemos luego unir B1 con A1, para así obtener C1.

```
 	  A---B---C topic
	 /         \
D---E---F---G---H master
Ejemplo sacado de la documentación oficial de git.
```

* **git merge nombreOtraRama**
    * Fusiona la rama actual (que suele ser master), con otra rama de nombre dado.

