---
title: "Introducción a la programación orientada a objetos"
slug: "introduccion-a-la-programacion-orientada-a-objetos"
description: "Introducción a la programación orientada a objetos"
date: 2022-03-06
publishdate: 2022-03-08
lastmod: 2022-03-08
draft: false
# Taxonomies
categories:
  - "Desarrollo"
authors:
  - "manuelperez"
# User-Defined
tableOfContents: true
---

Cuando se empieza a programar es imposible pasar por alto este **Paradigma de la programación**. El cual intenta _abstraer_ la realidad en la que vivimos a líneas de código. Suena interesante, ¿verdad? Pues quédate un poco más y sigue leyendo, que en este tema, te explicaré de forma resumida y fácil de entender qué es la programación orientada a objetos.

## 1. Origen
Sin entrar mucho en detalle, este paradigma se lo debemos, al lenguaje de programación [Simula 67](https://es.wikipedia.org/wiki/Simula). Un lenguaje diseñado para hacer simulaciones, creado por Ole-Johan Dahl y Kristen Nygaard, del Centro de Cómputo Noruego en Oslo. En este centro se trabajaba en simulaciones de naves. La idea surgió al agrupar los diversos tipos de naves en diversas clases de objetos, siendo responsable cada clase de objetos de definir sus "propios" datos y comportamientos. 

## 2. Realidad a código
Pues sí, señores y señoras, la POO busca eso precisamente. Imaginaros que tenemos un coche. ¿Qué propiedades tiene un coche? Yo diría que entre otras muchas, tiene ruedas, ventanillas, un color, un motor, una merca y un largo etcétera. Pero además, ¿Qué acciones puede realizar un coche? Bueno, puede arrancar, parar, acelerar, entre otras muchas.

¿Dónde entra aquí la programación orientada a objetos? Pues con ella, podemos *abstraer* esas cosas que tiene y hace un coche a clases. Esas clases son _moldes_ que nos sirve para crear *objetos* de la realidad en código. De esa forma podemos tener agrupado todo lo relativo a un coche en un trozo de código que podemos utilizar en nuestro código.

## 3. ¿Qué ventaja tiene la POO frente a la programación estructurada?
Por si no lo sabías antes de la existencia de la programación orientada a objeto, existía la programación estructurada. 

Este tipo de programación no tenía clases que _modelara_ la realidad a código, por tanto, no podíamos tener una clase que agrupara **atributos** y **métodos**, sino que teníamos variables y métodos sueltos por el código. Imaginaos lo lioso que eso podría llegar a ser esto. ¿Cuántas veces se te ha ido de manos un programa extenso? Pues ahora imagina que no hay nada que delimite que métodos y atributos van juntos. ¿Puedo invocar el método arrancar? ¿Pero cómo lo llamo? ¿Desde dónde? ¿Qué le paso? Todas esas preguntas son algo más fáciles de entender si sabemos que el método arrancar, pertenece a la clase Vehículo, ¿no?

## 4. Propiedades de la programación orientada a objetos
La POO tiene varias características que la diferencian de otros Paradigmas de programación. Pero hay algunos de ellos que son muy importantes. Para mi gusto, destacaría 4: herencia, polimorfismo, abstracción y encapsulamiento. Esas 4 caracterísiticas combinadas crean un paradigma realmente poderoso y versátil. 


### Abstracción
Como venimos mencionando a lo largo del artículo, la **abstracción** es la capacidad de llevar a código objetos de la vida real. Algo que tienes que tener en cuenta es que cuando hablamos de objetos no tiene porque ser solo cosas físicas. Un pensamiento, un sueño, etc., podrían ser modelados en código a través de una clase. Solo haría falta encontrar características y comportamientos comunes a, por ejemplo, todos los sueños.

Pero la abstracción no se queda solo ahí. También es la capacidad de una clase de tener un método que no está definido, para dejar que una subclase _(estamos hablando de **Herencia**)_ defina cómo se comporta dicho método.

### Herencia
Un BMW es un tipo de vehículo, ¿verdad? Pero una moto también es un tipo de vehículo. Un BMW es una marca, bastante concreto, mientras que una moto, un algo menos concreto, más generalizado y, por supuesto, un vehículo es algo todavía más generalizado, ¿cierto? Pero, al final, se podría hacer un gráfico de más generalizado a menos: 

- Vehículo -> coche -> BMW.
- Vehículo -> Moto -> Honda.

Como vemos, tanto moto, como coche son subtipos de vehículo. Ambos tendrán cosas en común como vehículos que son (tiene ruedas, motor, frenos, etc), pero también tendrán cosas diferentes. Un coche tiene 4 ruedas, un coche tiene puertas; una moto, por el contrario, tiene 2 ruedas y no tiene puertas.

Es aquí donde entra la herencia en la POO, que es la capacidad de definir **entidades/clases padres** que tienen atributos y comportamientos comunes, cuyos hijos podrán heredar y utilizar o incluso cambiar si así lo desean. 

Esta capacidad nos da la posibilidad de crear jerarquías de clases muy parecidas a la realidad, de forma que podemos _abstraer_ comportamientos y atributos comunes sin tener que replicarlos en cada clase.

### Polimorfismo
**Poli** significa muchos y **morfo** forma; es decir, la capacidad de tener muchas formas. En la POO esto se refiere a la capacidad de un objeto o método de tener varias formas.

Por ejemplo, hemos dicho que BMW es una clase, ¿no? Pero a su vez está tiene la forma de un coche y a su vez también tiene la forma de un vehículo. Por tanto, la clase BMW tiene la capacidad de cambiar de forma. Por otro lado, si tuviéramos un método no implementado en la clase Vehículo, este método podría ser implementado en cualquiera de sus subclases, permitiendo así que un método tuviera diferentes comportamientos (por ende, diferentes formas).

### Encapsulamiento
Aquí podemos hablar de dos conceptos: visibilidad y cohesión.

Si hablamos de encapsulamiento referente a la visibilidad, estamos hablando de la posibilidad de ocultar métodos y atributos a otras partes del código, de forma que por ejemplo, yo sé que un coche tiene un motor, que es un atributo, pero dicho atributo, es privado y solo es visible desde la clase Vehículo. Nadie fuera de dicha clase podrá acceder a ese motor; por tanto, sé que mi motor está seguro y que nadie hará mal uso de él.

Cuando hablamos de encapsulamiento referente a la cohesión, estamos hablando de la POO en sí, pues es la capacidad de encapsular comportamiento y atributos en una única clase, para mantener juntos y unidos lo que tiene mayor relación entre sí mismo.

## Epílogo
La Poo es mucho más de lo que se ha hablado aquí. Mucho más profunda y mucho más complicada, pero también mucho más bonita y potente de lo que en estas líneas de código se pueda dar a entender.

Espero que esta pequeña introducción te sirva no para comprender todo lo relativo a la POO, sino para adentrarte en este maravilloso mundo del paradigma de programación orientado a objetos.
 
