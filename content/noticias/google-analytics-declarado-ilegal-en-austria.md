---
title: "Google Analytics declarado ilegal en Austria"
#slug: ""
description: "La herramienta de analítica web gratuita de Google, Google Analytics, ha sido declarada ilegal en Austria como consecuencia de la retirada del acuerdo 'Privacy Shield' entre Europa y Estados Unidos."
date: 2022-02-16T20:04:00+01:00
publishdate: 2022-02-16T20:04:00+01:00
lastmod: 2022-02-16T20:04:00+01:00
draft: true
# Taxonomies
categories:
  - "Privacidad"
authors:
  - "josejfernandez"
# User-Defined
tableOfContents: false
---

Google Analytics es un sistema de análisis del tráfico para sitios web proporcionada de forma gratuita por Google. Cuando un sitio web utiliza Google Analytics, es capaz de obtener información sencilla de sus visitantes como el número de visitas por día, la cantidad de páginas vistas o si los visitantes acceden al sitio a través de un buscador o mediante un enlace, por ejemplo. Pero Google Analytics también ofrece acceso a **otros muchos datos**, que son enviados a Estados Unidos, y almacenados y procesados allí.

Durante más de dos décadas la transferencia de datos de los usuarios europeos hacia Estados Unidos ha estado regulada por acuerdos como [Safe Harbor](https://es.wikipedia.org/wiki/Principios_internacionales_safe_harbor), que existió desde el año 2000 hasta 2015, o [Privacy Shield](https://es.wikipedia.org/wiki/Escudo_de_la_Privacidad), que existió desde 2016 hasta 2020. Pero a mediados de 2020 [el Tribunal de Justicia de la Unión Europea anuló el acuerdo *Privacy Shield*](https://elpais.com/tecnologia/2020-07-16/la-justicia-europea-da-un-golpe-a-la-economia-digital-al-declarar-ilegal-el-acuerdo-entre-la-ue-y-ee-uu-para-transferir-de-datos.html). Europa mantiene desde entonces una [lista de países](https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_es) que considera ofrecen suficiente protección a la privacidad de los usuarios europeos.

Esto causó bastante incertidumbre entre aquellos sitios web que utilizaban las herramientas de Google y otras muchas que están ubicadas en Estados Unidos.

(En desarrollo)
