---
title: "Acerca de"
slug: "acerca-de"
---

## ¿Qué es 'Secretos del software'?

Nuestra objetivo es conseguir un Internet más libre, más seguro y más privado para todos, fuera del control de grandes empresas y gobiernos y basado en tecnología que respete nuestros derechos y nos ponga a nosotros, los usuarios, en el centro.

Queremos hacerlo **empoderándote a ti**, facilitándote los conocimientos que sólo están al alcance de unos pocos y difundiendo contenidos relevantes. Para ello escribimos y creamos contenidos que esperamos que lleguen a la mayor cantidad de usuarios posible, para concienciarles de lo que está en juego y conseguir que aquellos que tienen interés encuentren en nuestra web respuesta a sus preguntas.

Justicia, libertad, honestidad y transparencia son algunos de nuestros valores. Es muy fácil decir eso y es muy difícil estar a la altura, pero aquí va nuestro pequeño granito de arena en este espacio en Internet.

## ¿A quién va dirigida?

A usuarios sin muchos conocimientos técnicos. No escribimos ni hablamos para ingenieros, arquitectos o analistas, sino para gente normal que utiliza Internet en su día a día y que es víctima de la desinformación y de los intereses comerciales de grandes empresas, sin poder hacer demasiado al respecto.

Existen muchos sitios web en Internet sobre privacidad, pero todos ellos tienen una cosa en común: sus contenidos están pensados para usuarios con conocimientos técnicos. Nuestro enfoque es ligeramente distinto. Creemos que es preferible ayudar a los usuarios con menos conocimientos a repetir la misma información que ya hay disponible en la red para usuarios técnicos.

## ¿Quién está detrás?

Un equipo de voluntarios que podrás conocer en:

- Nuestro [repositorio de código](https://codeberg.org/josejfernandez/secretosdelsoftware.com)
- Nuestro [foro](https://foro.secretosdelsoftware.com/)
- Nuestro [canal de Telegram](https://t.me/secretosdelsoftware)

## ¿Puedo formar parte?

Claro. Lee nuestra sección de colaborar, aquí en el pie de página ⬇.
